---
# Base Preamble Settings
fontsize:
handout:
aspectratio:
language:
package:
    -
header-includes:
    -
include-before:
    -

# Title Font Variant
titleformat:
titleformat-title:
titleformat-subtitle:
titleformat-section:
titleformat-frame:

# Section Pages
sectionpage:
subsectionpage:

# Progress Bar
progressbar:

# Block
block:

# Color Definitions
color:
    - name:
      type:
      value:

# Color Settings
main-color:
background-color:
title-color:
progressbar-color:
alert-color:
structure-color:
block-title-color:
block-body-color:
box-color:

# Font Settings
sans-font:
mono-font:

# Frame Numbering
numbering:
    left:
    style:

# Custom Footer Content
footer:
    text:
    logo:

# Pandoc Integration
bold:
quote:
toc:
links-as-notes:
beamercovered:

# Personal Settings
title:
subtitle:
author:
    -
institute:
    -

# Event Information
event:
date:

# Logos
logo:
    -
logo-height:

# Final Highlights
highlights-title:
highlights-width:
highlights-rows:
    - frames:
        -

# Bibliography
biblio-style:
natbiboptions:
    -
bibliography:
    -
---
