% ----------------------------------------------------------------------------
% Pandoc Template for the Metropolis Beamer Theme.
% Copyright (c) 2019-2021 - Romain Wallon.
%
% The original Metropolis Theme is designed by Matthias Vogelgesang.
%
% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License (https://creativecommons.org/licenses/by-sa/4.0/).
% ----------------------------------------------------------------------------

% -------------------------- PACKAGE CONFIGURATION ---------------------------

\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}

% -------------------------- DOCUMENT CONFIGURATION --------------------------

\documentclass[%
    $if(fontsize)$$fontsize$$else$10pt$endif$,%
    $if(handout)$handout,$endif$%
    $if(aspectratio)$aspectratio=$aspectratio$,$endif$%
    usenames,%
    dvipsnames%
]{beamer}
\usetheme{metropolis}

% ----------------------------- PACKAGE LOADING ------------------------------

% Language.
\usepackage[$if(language)$$language$$else$english$endif$]{babel}

% Tables.
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{tabularx}

% Figure customization.
\usepackage{graphicx,grffile}
\usepackage{float}

% Syntax highlighting.
\usepackage{listings}

% Appendix management.
\usepackage{appendixnumberbeamer}

% User-specific packages.
$for(package)$
\usepackage{$package$}
$endfor$

% ------------------------- METROPOLIS CUSTOMIZATION -------------------------

$if(titleformat)$
\metroset{titleformat=$titleformat$}
$endif$

$if(titleformat-title)$
\metroset{titleformat title=$titleformat-title$}
$endif$

$if(titleformat-subtitle)$
\metroset{titleformat subtitle=$titleformat-subtitle$}
$endif$

$if(titleformat-section)$
\metroset{titleformat section=$titleformat-section$}
$endif$

$if(titleformat-frame)$
\metroset{titleformat frame=$titleformat-frame$}
$endif$

$if(sectionpage)$
\metroset{sectionpage=$sectionpage$}
$endif$

$if(subsectionpage)$
\metroset{subsectionpage=$subsectionpage$}
$endif$

$if(progressbar)$
\metroset{progressbar=$progressbar$}
$endif$

$if(block)$
\metroset{block=$block$}
$endif$

% --------------------------- COLOR CONFIGURATION ----------------------------

$for(color)$
\definecolor{$color.name$}{$color.type$}{$color.value$}
$endfor$

$if(main-color)$
\setbeamercolor{frametitle}{bg=$main-color$}
\setbeamercolor{normal text}{fg=$main-color$}
$endif$

$if(background-color)$
\setbeamercolor{background canvas}{bg=$background-color$}
$endif$

$if(title-color)$
\setbeamercolor{frametitle}{fg=$title-color$}
$endif$

$if(progressbar-color)$
\setbeamercolor{progress bar}{fg=$progressbar-color$}
\setbeamercolor{progress bar in section page}{fg=$progressbar-color$}
\setbeamercolor{progress bar in head/foot}{fg=$progressbar-color$}
$endif$

$if(alert-color)$
\setbeamercolor{alerted text}{fg=$alert-color$}
$endif$

$if(structure-color)$
\setbeamercolor{structure}{fg=$structure-color$}
$endif$

$if(block-title-color)$
\setbeamercolor{block title}{bg=$block-title-color$}
$endif$

$if(block-body-color)$
\setbeamercolor{block body}{bg=$block-body-color$}
$endif$

$if(box-color)$
\beamerboxesdeclarecolorscheme{punchline}{$if(main-color)$$main-color$$else$mDarkTeal$endif$}{$box-color$}
$else$
\definecolor{mLightGray}{HTML}{E8E8E8}
\beamerboxesdeclarecolorscheme{punchline}{$if(main-color)$$main-color$$else$mDarkTeal$endif$}{mLightGray}
$endif$

% -------------------------------- FONT STYLE --------------------------------

$if(sans-font)$
\setsansfont{$sans-font$}
$endif$

$if(mono-font)$
\setmonofont{$mono-font$}
$endif$

% ------------------------------- TITLE SLIDE --------------------------------

\renewcommand{\titlepage}{
    \begin{minipage}[c][\paperheight]{\textwidth}
        \ifx\inserttitle\@empty\else\usebeamertemplate*{title}\fi
        \ifx\insertsubtitle\@empty\else\usebeamertemplate*{subtitle}\fi

        \usebeamertemplate*{title separator}

        \ifx\beamer@shortauthor\@empty\else\usebeamertemplate*{author}\fi
        \ifx\insertdate\@empty\else\usebeamertemplate*{date}\fi
        \ifx\insertinstitute\@empty\else\usebeamertemplate*{institute}\fi

        \vspace*{10mm}

$if(logo)$
        \begin{tabularx}{\textwidth}{$for(logo)$Y$endfor$}
$for(logo)$
            \includegraphics[height=$if(logo-height)$$logo-height$$else$15mm$endif$]{$logo$}$sep$&
$endfor$
        \end{tabularx}
$endif$
    \end{minipage}
}

% ------------------------------- SLIDE FOOTER -------------------------------

% General footer style.
\setbeamertemplate{footline}{
    \begin{beamercolorbox}[wd=\textwidth, sep=3ex]{footline}
        \usebeamerfont{page number in head/foot}
$if(numbering.left)$
        % Frame numbering is put on the left of the slides.
        \usebeamertemplate*{frame numbering}
        \hfill
        \usebeamertemplate*{frame footer}
$else$
        % Frame numbering is put on the right of the slides.
        \usebeamertemplate*{frame footer}
        \hfill
        \usebeamertemplate*{frame numbering}
$endif$
    \end{beamercolorbox}
}

% Frame numbering style.
$if(numbering.style)$
\metroset{numbering=$numbering.style$}
$else$
\metroset{numbering=fraction}
$endif$

% Customizing footer content.
$if(footer.text)$
\setbeamertemplate{frame footer}{$footer.text$}
$else$
$if(footer.logo)$
\setbeamertemplate{frame footer}{\includegraphics[height=0.6cm]{$footer.logo$}}
$endif$
$endif$

% ------------------------ BIBLIOGRAPHY CUSTOMIZATION ------------------------

$if(bibliography)$
\usepackage[$for(natbiboptions)$$natbiboptions$$sep$,$endfor$]{natbib}
\bibliographystyle{$if(biblio-style)$$biblio-style$$else$plainnat$endif$}
$endif$

% ---------------------------- PANDOC INTEGRATION ----------------------------

$if(bold)$
% Bold font is left as is.
$else$
% Bold font is replaced by Alert font.
\renewcommand{\textbf}[1]{\alert{#1}}
$endif$

$if(quote)$
% Quote environment is left as is.
$else$
% Quote environment is redefined to be used for punchlines.
\renewenvironment{quote}{
    \bigskip
    \begin{beamerboxesrounded}[scheme=punchline,shadow=true]{}
        \centering
        \em
}{
    \end{beamerboxesrounded}
}
$endif$

% Various style settings.
\newcolumntype{Y}{>{\centering\arraybackslash}X}
\providecommand{\tightlist}{\setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

$if(links-as-notes)$
% Links are considered as footnotes.
\DeclareRobustCommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$

$if(beamercovered)$
% Modifying the style of hidden elements.
\setbeamercovered{$beamercovered$}
$endif$

% --------------------------- SYNTAX HIGHLIGHTING ----------------------------

\newcommand{\passthrough}[1]{#1}

\lstset{defaultdialect=[5.3]Lua}
\lstset{defaultdialect=[x86masm]Assembler}

$highlighting-macros$

% ---------------------------- FIGURE POSITIONING ----------------------------

\makeatletter

% Maximum width for the figures.
\def \maxwidth {
    \ifdim \Gin@nat@width > \linewidth
        0.9\linewidth
    \else
        \Gin@nat@width
    \fi
}

% Maximum height for the figures.
\def \maxheight {
    \ifdim \Gin@nat@height > \textheight
        0.7\textheight
    \else
        \Gin@nat@height
    \fi
}

\makeatother

% Setting up figures to fit into the slides.
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}

% ----------------------------- CUSTOM PREAMBLE ------------------------------

$for(header-includes)$
$header-includes$
$endfor$

$for(include-before)$
$include-before$
$endfor$

% -------------------------- PRESENTATION METADATA ---------------------------

\title{$title$}

$if(subtitle)$
\subtitle{$subtitle$}
$endif$

\date{$if(event)$$event$ -- $endif$$if(date)$$date$$else$\today$endif$}

\author{$for(author)${$author$}$sep$, $endfor$}
\institute{$for(institute)$$institute$$sep$\\$endfor$}

% ---------------------------------- DOCUMENT --------------------------------

\begin{document}

% The opening slide.
\maketitle

$if(toc)$
% The slide showing the table of contents.
\begin{frame}{\contentsname}
    \setbeamertemplate{section in toc}[sections numbered]
    \tableofcontents[hideallsubsections]
\end{frame}
$endif$

% The actual content of the presentation.
$body$

$if(highlights)$
% The highlights of the talk, as a conclusion.
\begin{frame}$if(highlights-title)$$else$[plain]$endif$
$if(highlights-title)$
    \frametitle{$highlights-title$}
$endif$
$if(no-highlights)$
$else$
    \setlength{\tabcolsep}{10pt}
$for(highlights-rows)$
    \begin{tabularx}{\textwidth}{YYYY}
$for(highlights-rows.frames)$
        \includegraphics[%
            width=$if(highlights-width)$$highlights-width$$else$3cm$endif$,%
            page=$highlights-rows.frames$%
        ]{%
            $highlights$%
        }$sep$&
$endfor$
    \end{tabularx}
$endfor$
$endif$
\end{frame}
$endif$

% The closing slide.
\maketitle

$if(backup)$
% The backup slides.
\appendix
\input{$backup$}

% The "real" closing slide.
\maketitle
$endif$

$if(bibliography)$
% Configuring the style for the bibliography.
\metroset{sectionpage=none}
\metroset{subsectionpage=none}
\setbeamertemplate{frametitle continuation}{}

% Showing the bibliography.
\begin{frame}[allowframebreaks,noframenumbering]{\bibname}
    \bibliography{$for(bibliography)$$bibliography$$sep$,$endfor$}
\end{frame}
$endif$

\end{document}
