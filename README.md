# Pandoc Template for the *Metropolis* Beamer Theme

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/metropolis/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/metropolis/commits/master)

## Description

This project provides a template allowing an easy customization of the
[*Metropolis* Beamer Theme](https://github.com/matze/mtheme) for a smooth
integration with [Pandoc](https://pandoc.org).

This template, and in particular the bundled [`sty`](./sty) files of
*Metropolis*, is distributed under a Creative Commons Attribution-ShareAlike
4.0 International License.

[![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

## Requirements

To build your presentation using this template, [Pandoc](https://pandoc.org)
at least 2.0 and [LaTeX](https://www.latex-project.org/) have to be installed
on your computer.

*Metropolis* (as-is from [this](https://github.com/matze/mtheme/commit/2fa6084)
commit) is bundled with this template, so you are not required to install it.

To get the best from this theme, you will need XeLaTeX, which should be bundled
with your LaTeX distribution.
If not, you may want to install it, or to set the variable `PDF_ENGINE` to
`pdflatex` when building (see below).

If you want to have the original font of *Metropolis*, you will need to install
[Mozilla's Fira font](https://github.com/mozilla/Fira).
If this font is not available, an other *sans-serif* font will be used.

## Creating your Presentation

This template enables to write amazing *Metropolis* presentations in plain
[Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), thanks to the
power of Pandoc.

To create your presentation, the easiest way is to clone this repository (e.g., 
as a git submodule), and set `METROPOLIS_HOME` to the path of your clone when
building (see below).
You may also put the `sty` directory, the `Makefile` and the template file
`metropolis.pandoc` inside the directory in which you have your Markdown source
file and the (optional) metadata file.

The template we provide supports many customizations, so that you can get the
best of *Metropolis* without having to write LaTeX code.

You will find below a description of the available customizations.
You may also be interested in our [examples](./examples), which illustrate
various use cases.

### About the Title Slide

First, note that our template does not use *exactly* the title slide defined by
*Metropolis*.
For an easier customization, logos are put in a table, so that multiple images
can easily be used.
This table is put at the **bottom** of the title slide.

Moreover, this slide is also used as *closing* slide, as commonly advised
for presentations.

### YAML Configuration

Most customizations are set in the YAML configuration.
You are free to put this configuration in a separate file, or at the top of
your Markdown file, provided that you set the variable `METADATA` accordingly
when building.

For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones you do not need.
Read below for details on how to configure your presentation, and see our
various [examples](./examples) for different use cases.

> *Nota-Bene*
>
> Some variables used in the configuration are used as *Boolean variables*.
> However, Pandoc does not allow to *check* the actual content of a variable.
> As a consequence, we can only check whether a variable exists or not.
>
> This means that, if a variable is set to `false`, it will still be considered
> as being set, and it will not have the expected effect.
>
> That is why you must consider such variables as *flags*, that you set only
> if you need them, preferably to `true` for better readability.
>
> In the following, the description of flag variables always has the form
> "if present, ...".

#### Base Preamble Settings

Some of the base settings can be set with the following variables:

+ `fontsize`: sets the size of the font, among `8pt`, `9pt`, `10pt`, `11pt`,
  `12pt`, `14pt`, `17pt` and `20pt` (default is `10pt`).
+ `handout`: if present, produces slides without overlays.
+ `aspectratio`: sets the ratio of the size of the slides, among `1610`, `169`,
  `149`, `54`, `43` and `32`, which stand for the ratios 16:10, 16:9, 14:9, 5:4,
  4:3 and 3:2, respectively.
+ `language`: sets the language to be used by `babel` (default is `english`).
+ `package`: the list of packages to include in the presentation.
+ `header-includes`: the list of all other LaTeX commands you want to add to
  the preamble.
+ `include-before`: the list of all other LaTeX commands you want to add to the
  preamble (prefer the use of `header-includes`, to preserve Pandoc's original
  semantic for this variable).

#### *Metropolis* Settings

The *Metropolis* package has a lot of options to help you customize your
presentation.
Most of them can be set from the YAML configuration we provide.

##### Title Font Variant

You may specify the font variant to use for the various titles of your
presentation with the following variables:

+ `titleformat`: sets the variant for all titles.
+ `titleformat-title`: sets the variant for the title of the presentation.
+ `titleformat-subtitle`: sets the variant for the subtitle of the
  presentation.
+ `titleformat-section`: sets the variant for the title of each section.
+ `titleformat-frame`: sets the variant for the title of each slide.

Note that all these variables take their value among `regular`, `smallcaps`,
`allsmallcaps` and `allcaps`.
Their default value is `regular`.

##### Section Pages

You may add a title slide for each section or subsection by setting the
variables `sectionpage` and `subsectionpage` to one of `none`, `simple`, or
`progressbar`.

By default, `sectionpage` is set to `progressbar` and `subsectionpage` to
`none`.

##### Progress Bar

If you wish, you may put a progress bar on each slide by setting `progressbar`
to `head`, `frametitle`, or `foot`.
The default value is `none` (i.e., there is no progress bar).

##### Blocks

If you want to use blocks in your presentation, you may customize their
appearance by setting the variable `block` to either `fill` or `transparent`
(default is `transparent`).

Note that Beamer blocks are not natively supported by Pandoc, but you may use
[a filter](https://gitlab.com/pandoc-toolkit-by-rwallon/filters/-/blob/master/beamerblocks.lua)
to still write blocks in plain Markdown.

#### Color Settings

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

Once you have defined your own colors, you may set the following properties
to customize the colors of your presentation (of course, you may also use
predefined colors):

+ `main-color`: the color for the text and for the background of slide titles.
+ `background-color`: the color for the background of the slides.
+ `title-color`: the color for the slide titles.
+ `progressbar-color`: the color for the progress bars.
+ `alert-color`: the color for alerted text.
+ `structure-color`: the color for structure elements.
+ `block-title-color`: the color for the background of block titles.
+ `block-body-color`: the color for the background of block bodies.
+ `box-color`: the background color for the boxes used for *punchlines*.

Note that *Metropolis* actually allows to set more colors than the ones we
propose in this template, but we deliberately chose not to use them all, to
avoid having too many different colors on the slides.
Actually, at most five different colors should be used.

#### Font Settings

You may set the sans-serif font and monospace font used in the slides by
setting `sans-font` and `mono-font` accordingly.

#### Slide Footer Settings

Slide footer may contain up to two elements.

##### Frame Numbering

The number of each slide appears at the bottom of the slides.
You may customize this numbering by specifying the two attributes of
the variable `numbering`:

+ `left`: if present, puts the frame numbering at the left of the footer
  (it is put on the right by default).
+ `style`: defines how frame are numbered, among `none`, `counter`, and
  `fraction` (default is `fraction`, and not `counter` as in the original
  *Metropolis*).

##### Custom Footer Content

You may specify your own content for the footer by setting one of the two
attributes of the variable `footer`:

+ `text`: the text to put in the footer.
+ `logo`: the path of the logo to put in the footer (ignored if `text` is set).

This content will appear in the opposite corner of the one used for frame
numbering.

#### Pandoc Integration

As Markdown does not offer as many features as LaTeX, our template redefines
some styling of *Metropolis*.

First, any **bold** text will actually be interpreted as **alerted** text.
You may restore the original behavior by putting `bold: true` in the metadata.

Second, **quotes** are interpreted as **punchlines**, which are highlighted
blocks of text used for *plot twists* or *take-away* messages.
Once again, you may restore the original behavior by setting `quote: true` in
the metadata.

Additionally, if you want to add an overview (table of contents) of your
presentation on your second slide, put `toc: true` in the metadata.

You can also add `links-as-notes: true` in the metadata to make URLs appear as
footnotes in the slides, instead of having hyperlinks in the content of the
slides (which is not practical for attendees if they cannot access the slides).

Last but not least, you may configure the style for overlays by setting the
variable `beamercovered` to one of `dynamic`, `invisible` or `transparent`.

#### Personal Settings

To write your *own* presentation, you need to set the following variables:

+ `title`: the title of the presentation.
+ `subtitle`: the subtitle of the presentation (optional).
+ `author`: the name of the author of the presentation (use a list if there is
  more than one author).
+ `institute`: the name of the author's affiliation (use a list if there is
  more than one affiliation).
+ `event`: the event in which the presentation takes place (optional).
+ `date`: the date on which the presentation takes place (optional, default is
  the current date).

You may also want to add the logo(s) of the affiliation(s) to the title
slide.
This can be achieved by setting `logo` to the list of the images (given by
their path) to use as logos.
If the images are too big, you may also set `logo-height` to change their
height.
The slide width is proportionally divided between each logo.

#### Final Highlights

A possible way of ending a presentation is to use *miniature slides*, as
proposed by [Andreas Zeller](https://andreas-zeller.info/2013/10/22/summarizing-your-presentation-with.html).
Our template provides an easy and automated way to do so through the YAML
configuration.

First, you may specify the title of the slide containing the miniatures
with `highlights-title`.
By default, there is no title for this slide (in this case, miniatures are
displayed in a *plain* frame).

To specify the miniature slides you want to show, you may add something
similar to the following in the YAML configuration.

```yaml
highlights-rows:
    - frames:
        - 9
        - 13
    - frames:
        - 14
        - 17
```

This will show the miniatures of pages #9 and #13 in the first row, and the
miniatures of pages #14 and #17 in the second row.
You may show up to four miniatures in a row, in any number of rows.
Note that the specified numbers are those of the **pages** in the PDF, and
**not those of the slides**, as they may be different if you use Beamer
animations, for instance.

Finally, you may force the width of the miniatures if they do not fit well in
the slide by setting `highlights-width` accordingly.

#### Bibliography

Bibliography references are managed with `natbib` and `pandoc-citeproc`.
In this section, we do not enter too much into the details on how to use these.
A complete documentation about citations and Pandoc may be found
[here](https://pandoc.org/MANUAL.html#citations).

The following settings may be specified to handle references in your
presentation:

- `biblio-style`: the style of the bibliography.
- `natbiboptions`: the list of options to pass to the `natbib` package.
- `bibliography`: the list of BibTeX files, given by their paths.

#### Backup Slides

Backup slides are useful when you want to keep slides for a question-and-answer
session after your talk or for further discussion afterwards.

These slides are put *after* the "official" closing slide, and *before* another
"real" closing slide.
These slides are **not numbered**, so as to keep them "secret".

Backup slides must be written in *Markdown* as the rest of your presentation,
but in a separate file.
To build them, you need to set the variable `BACKUP` in the `Makefile` to the
name of this separate file (as you did, e.g., for your main source file).

### Building your Presentation

Presentations based on this template are built using `make`.
To customize the build to your own use case, you may either update a copy of
the [`Makefile`](./Makefile) provided in this project, or set the needed
variables in the command line.
Since version `0.3.0`, the preferred solution is the latter one, as it allows
to reuse a single copy of this template accross multiple presentations.
This section shows how to do so.

Suppose for example that this template is located at `path/to/metropolis`,
your Markdown source file is `example.md`, your metadata file is `metadata.md`,
your backup slides are written in `backup.md` and the titles of your slides
correspond to level-3 titles in your Markdown source file.
You will then have to type the following command to build your presentation:

```bash
make -f path/to/metropolis/Makefile METROPOLIS_HOME=path/to/metropolis METADATA=metadata.md FILENAME=example BACKUP=backup SLIDE_LEVEL=3
```

Also, if you want to add miniature slides for the highlights of your
presentation, you must add `HIGHLIGHTS=T` to the command line, as highlights
require a particular building.

Note that you may also set the program to use to create the PDF from LaTeX
source with `PDF_ENGINE`, and customize the way Pandoc produces the slides by
setting the variable `PANDOC_OPTS` accordingly.
For more details, read [Pandoc's User Guide](https://pandoc.org/MANUAL.html).

Our `Makefile` also provides a `clean` target, which removes all generated
files but the PDF of the slides, and a `mrproper` target, which also removes
this PDF.

The target `help` gives the full list of available targets.
