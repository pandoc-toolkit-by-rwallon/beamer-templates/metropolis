#!/bin/bash
#
# Builds examples of the Pandoc Template for Metropolis.
#
# $1 - The name of the metadata file containing the configuration of the
#      example to build.

############
# FUNCTION #
############

# Builds the specified example.
#
# Arguments:
#     $1 - The name of the example to build.
#
# Returns:
#     None
function build {
    make -f ../Makefile SLIDE_LEVEL=3 HIGHLIGHTS=T METROPOLIS_HOME=.. METADATA="$1.md" FILENAME=example BACKUP=backup
    test -e example.pdf && mv example.pdf "$1.pdf"
    make -f ../Makefile FILENAME=example clean
}

########
# MAIN #
########

# Checking the command line arguments.
test $# -ne 1 && echo "Usage: $0 <metadata-file>" && exit 1
test ! -f "$1" && echo "$0: $1: No such file or directory" && exit 2

# Getting the name of the example to build.
example=$(basename "$1" ".md")

# Building the example.
build "$example"

# Checking whether the build succeeded.
test ! -e "$example.pdf" && exit 3
exit 0
