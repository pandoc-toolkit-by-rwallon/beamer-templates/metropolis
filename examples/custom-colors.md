---
# Progress Bar
progressbar: frametitle

# Color Definitions
color:
    - name: myblue
      type: HTML
      value: 80ADD7
    - name: mygreen
      type: HTML
      value: 0ABDA0
    - name: mywhite
      type: HTML
      value: EBF2EA
    - name: myspring
      type: HTML
      value: D4DCA9
    - name: mybrown
      type: HTML
      value: BF9D7A

# Color Settings
main-color: myblue
background-color: mywhite
progressbar-color: mybrown
alert-color: mygreen
structure-color: mybrown
box-color: myspring
---
