---
# Personal Settings
title: Pandoc Template for Metropolis
subtitle: A Modern Beamer Theme
author: Romain Wallon
institute: Pandoc Toolkit

# Event Information
event: Release 0.4.0
date: December 2021

# Logos
logo:
    - resources/markdown.png
    - resources/latex.png
logo-height: 1.4cm

# Pandoc Integration
links-as-notes: true

# Final Highlights
highlights-title: Talk Highlights as a Conclusion
highlights-width: 4.5cm
highlights-rows:
    - frames:
        - 9
        - 13
    - frames:
        - 14
        - 17

# Bibliography
bibliography: resources/bibliography.bib
---

# Introduction

## From Metropolis to Pandoc

### Metropolis

The **Metropolis** theme is a Beamer theme with minimal visual noise inspired
by the `HSRM` Beamer Theme by Benjamin Weiss

The *Metropolis* theme itself is developed by Matthias Vogelgesang

Note that you have to have Mozilla's *Fira Sans* font and `XeLaTeX` installed
to enjoy this wonderful typography

### Pandoc

**Pandoc** is a Haskell library for converting from one markup format to
another, and a command-line tool that uses this library

### Why a Pandoc Template for Metropolis?

To use *Metropolis* with Pandoc-based presentations, you may run the following
command:

```bash
$ pandoc --pdf-engine=xelatex \
         -t beamer -V theme:metropolis \
         -o output.pdf input.md
```

However, this often requires to write `LaTeX` [@latex] code if you want to
customize your presentation

We provide instead a highly-customizable Pandoc template for *Metropolis*, to
allow to write slides in plain *Markdown* [@markdown]

# Writing your Presentation

## Organization

### Sections and Subsections

Sections and subsections group slides of the same topic

```markdown
# A Section

## A Subsection
```

*Metropolis* provides a nice progress indicator for them...

### Titles

*Metropolis* supports 4 different title formats:

+ `regular`
+ `smallcaps`
+ `allsmallcaps`
+ `allcaps`

Contrary to the original *Metropolis*, you can only set the format **once and
for all**, unless you write `LaTeX` code

## Elements

### Typography

Because Markdown is limited in terms of typography, you cannot combine bold and
alert fonts:

```markdown
The theme provides sensible defaults to
*emphasize* text, and to show **strong**
results
```

becomes

The theme provides sensible defaults to *emphasize* text, and to show
**strong** results

### Lists

There is a support for items:

+ Milk
+ Eggs
+ Potatoes

Enumerations are also supported:

1. First
2. Second
3. Last

And so are definition lists:

Foo

:    This is the definition of Foo

Bar

:    This is the definition of Bar

Baz

:    This is the definition of Baz

### Animations

Unfortunately, Markdown does not support Beamer animations...

. . .

But Pandoc does!

. . .

Simply put `. . .` in your presentation, and it will work perfectly!

### Figures

![This presentation is written in Mardown](resources/markdown.png)

### Tables

Tables are supported through Pandoc's Markdown

The largest cities in the world (source: Wikipedia) are:

| City         | Population  |
|--------------|-------------|
| Mexico City  | 20,116,842  |
| Shanghai     | 19,210,000  |
| Peking       | 15,796,450  |
| Istanbul     | 14,160,467  |

### Punchlines

*Punchlines* are implemented through the `quote` environment, unless you
specified that you did not want them:

```markdown
> Isn't it great?
```

They are used for **plot-twists** in your presentations, or for **take-away
messages**

. . .

> Isn't it great?

# Conclusion

### Conclusion

Get the source of the theme on [GitHub](https://github.com/matze/mtheme)

Get the source of the template and all the examples on
[GitLab](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/metropolis)

> Both the theme and the template are licensed under a Creative Commons
> Attribution-ShareAlike 4.0 International License
