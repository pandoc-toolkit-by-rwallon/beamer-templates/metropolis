# Use Cases of the *Metropolis* Template

This directory contains examples illustrating different use cases of the Pandoc
template for the *Metropolis* Beamer theme.

All the examples are based on the same Markdown source, available
[here](example.md) (with an additional *backup slide* [here](backup.md)).
The different outputs are simply obtained by using different metadata files.

Observe that, in these examples, slide titles are level-3 titles in the
Markdown source files.
Hence, `SLIDE_LEVEL` has to be set to `3` in the `Makefile` when building the
presentations.

For each of the examples, you may either read its metadata file (to see how we
configured it) or download the final PDF.

## Default Configuration

In this example, there are no additional settings.
The presentation is produced based on the default configuration.

*Metadata file available [here](default.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/default.pdf?job=make-examples).*

## Original *Metropolis*

This example is configured so that the presentation uses the original default
configuration of the *Metropolis* theme.

A custom text footer is also added.

*Metadata file available [here](original.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/original.pdf?job=make-examples).*

## Custom Layout

The configuration for all the titles appearing in the presentation is modified,
and title pages for both sections and subsections are added, without any
progress bar on these slides.
Instead, a progress bar is put on the footer of each of the other slides,
along with a logo and the frame numbering on the left.

The size of the font is set to `11pt`, and the size of the slides is set to the
ratio 16:10.

Also, the table of contents appears on slide 2, and the style of overlays is
set to `dynamic`.

*Metadata file available [here](custom-layout.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/custom-layout.pdf?job=make-examples).*

## Custom Colors

This example does not use the original colors of *Metropolis*, but a set of
user-defined colors instead.

Also, a progress bar is put under the title of each slide.

*Metadata file available [here](custom-colors.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/custom-colors.pdf?job=make-examples).*
